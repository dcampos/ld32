package net.deltaplay.ld32.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.deltaplay.ld32.LD32;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "LD32";
		config.width = 1000;
		config.height = 480;
		config.resizable = false;
		new LwjglApplication(new LD32(), config);
	}
}
