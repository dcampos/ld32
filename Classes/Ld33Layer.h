#ifndef LD33_H
#define LD33_H

#define COCOS2D_DEBUG 1

#include "cocos2d.h"
#include "GameSprite.h"
#include "Bird.h"
#include "Scarecrow.h"

USING_NS_CC;

class Ld33Layer : public cocos2d::Layer {
  public:
    static cocos2d::Scene *createScene();
    virtual bool init() override;
    void update(float) override;

  private:
    GameSprite *background, *tomato;
    Scarecrow *scarecrow;
    Vector<Bird*> crows;
    Vec2 lastSpawnPt;
    float spawnTime;

    void createSprites();
    void spawnBird();

    CREATE_FUNC(Ld33Layer);
};

#endif /* LD33_H */
