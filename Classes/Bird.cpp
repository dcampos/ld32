#include "Bird.h"

void Bird::update(float dt) {
    Point position = getPosition();

    position.x -= dt * speed.x;
    position.y -= dt * speed.y;

    setPosition(position);

}

Bird *Bird::createWithSpriteFrameName(const char *spriteName) {
    auto sprite = new Bird();

    if (sprite && sprite->initWithSpriteFrameName(spriteName)) {
        sprite->speed.set(80, 0);
        sprite->autorelease();
        return sprite;
    }

    CC_SAFE_DELETE(sprite);
    return nullptr;
}
