#include "Scarecrow.h"

Scarecrow *Scarecrow::create() {
    auto sprite = new Scarecrow();

    if (sprite && sprite->initWithSpriteFrameName("scarecrow.png")) {
        sprite->autorelease();
        return sprite;
    }

    CC_SAFE_DELETE(sprite);
    return nullptr;
}
