#ifndef SCARECROW_H
#define SCARECROW_H

#include "GameSprite.h"

class Scarecrow : public GameSprite {
  public:
    static Scarecrow *create();
};

#endif /* SCARECROW_H */
