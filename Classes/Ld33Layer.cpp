#include <iostream>
#include "Ld33Layer.h"
#include "GameSprite.h"
#include "Bird.h"

USING_NS_CC;

Scene *Ld33Layer::createScene() {
    auto scene = Scene::create();
    auto layer = Ld33Layer::create();

    scene->addChild(layer);

    return scene;
}

bool Ld33Layer::init() {
    if (!Layer::init()) {
        return false;
    }

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("normal.plist");

    auto label = Label::createWithSystemFont("Ld33!", "Arial", 80);
    label->setAnchorPoint(cocos2d::Vec2::ANCHOR_BOTTOM_LEFT);
    this->addChild(label, 1);

    createSprites();

    scheduleUpdate();

    return true;
}

void Ld33Layer::update(float delta) {
    spawnTime += delta;
    if (spawnTime > 5) {
        CCLOG("spawnTime=%f", spawnTime);
        spawnBird();
        spawnTime = 0;
    }


    for (int i = 0; i < crows.size(); ++i) {
        Bird *bird = crows.at(i);
        bird->update(delta);

        if (!getBoundingBox().getMinX() > bird->getBoundingBox().getMaxX()) {
            crows.erase(i);
            removeChild(bird);
            CCLOG("%s", "Bird removed!");
            spawnBird();
        }
    }
}

void Ld33Layer::spawnBird() {
    Bird *bird = Bird::createWithSpriteFrameName("crow1.png");
    bird->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    float randY = random(0, 480);

    bird->setPosition(Vec2(getBoundingBox().getMaxX(), randY));

    if (bird->getBoundingBox().intersectsCircle(
            lastSpawnPt, bird->getBoundingBox().size.width)) {
        Point position = bird->getPosition();
        position.y += (randY > lastSpawnPt.y ? 200 : -200);
        bird->setPosition(position);
        CCLOG("randy=%f, pos.y=%f", randY, position.y);
    }

    lastSpawnPt = bird->getPosition();

    crows.pushBack(bird);
    this->addChild(bird, 1);
}

void Ld33Layer::createSprites() {
    background = GameSprite::createWithSpriteFrameName("background.png");
    background->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    this->addChild(background, 1);

    scarecrow = Scarecrow::create();
    scarecrow->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    scarecrow->setPosition(Vec2(100, 100));
    this->addChild(scarecrow, 1);

    spawnBird();
    spawnBird();
}
