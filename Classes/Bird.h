#ifndef BIRD_H
#define BIRD_H

#include "GameSprite.h"

class Bird : public GameSprite {
  private:
  public:
    CC_SYNTHESIZE(Vec2, speed, Speed);

    static Bird *createWithSpriteFrameName(const char* spriteName);
    void update(float dt) override;
};

#endif /* BIRD_H */
