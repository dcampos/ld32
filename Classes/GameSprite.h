#ifndef GAMESPRITE_H
#define GAMESPRITE_H

#include "cocos2d.h"

USING_NS_CC;

class GameSprite : public Sprite {
  public:

    static GameSprite *createWithSpriteFrameName(const char *pszSpriteFrameName);

};

#endif /* GAMESPRITE_H */
