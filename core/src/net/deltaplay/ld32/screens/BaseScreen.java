package net.deltaplay.ld32.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import net.deltaplay.ld32.DisplayUtils;
import net.deltaplay.ld32.LD32;
import net.deltaplay.ld32.Log;

public abstract class BaseScreen implements Screen {
    protected final LD32 game;

    protected Stage stage;

    protected Dialog dialog;

    protected Table outerTable;
    protected Table table;

    public BaseScreen(LD32 g, boolean layout) {
        this.game = g;

        //stage = new Stage(new ExtendFitViewport(480, 320), game.getBatch());
        stage = new Stage(new FitViewport(LD32.SCREEN_WIDTH, LD32.SCREEN_HEIGHT), game.batch);

        this.outerTable = new Table();
        this.table = new Table();

        if (layout) {
            table.columnDefaults(0).pad(DisplayUtils.sizeToDpi(10));

            outerTable.setFillParent(true);

            outerTable.add(table);

        }

        stage.addActor(outerTable);

        init();

        System.out.println("getScreenWidth=" + stage.getViewport().getScreenWidth());
        System.out.println("getWorldWidth=" + stage.getViewport().getWorldWidth());

        /*
         * 
         */
        layout();

    }

    /**
     * Should be called by the concrete classes when the stage is resized to
     * allow them to reset their layout and call the super() method.
     */
    private void layout() {

    }

    /*
     * Initialize concrete classes
     */
    protected abstract void init();

    public Stage getStage() {
        return stage;
    }

    public Game getGame() {
        return game;
    }

    public FrameBuffer renderToBuffer() {
        FrameBuffer buffer = new FrameBuffer(Pixmap.Format.RGB565, (int) getStage().getWidth(), (int) getStage().getHeight(), false);

        buffer.begin();
        render(Gdx.graphics.getDeltaTime());
        buffer.end();

        return buffer;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
        // Table.drawDebug(stage);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        Vector2 size = Scaling.fit.apply(LD32.SCREEN_WIDTH, LD32.SCREEN_HEIGHT, width, height);

        Cell cell =  outerTable.getCell(table);
        if (cell != null) cell.size(size.x, size.y);

        //table.setPosition((stage.getWidth() - size.x) / 2, 0);
    }

    @Override
    public void show() {
        //Gdx.input.setInputProcessor(stage);
        Log.log(BaseScreen.class, "show() called");
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
