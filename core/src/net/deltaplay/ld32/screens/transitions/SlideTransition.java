package net.deltaplay.ld32.screens.transitions;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Pools;

/**
 * Created by dpc on 23/03/15.
 */
public class SlideTransition extends Transition {
    boolean left;

    @Override
    public void before() {
        prevBuffer = previousScreen.renderToBuffer();
    }

    @Override
    public void after() {
        TextureRegion region = new TextureRegion(prevBuffer.getColorBufferTexture());
        region.flip(false, true);
        Image img = new Image(region);
        nextScreen.getStage().addActor(img);
        img.toFront();

        float amount = left ? -region.getRegionWidth() : region.getRegionWidth();
        img.addAction(Actions.sequence(Actions.moveBy(amount, 0, 0.5f), Actions.run(new Runnable() {
            @Override
            public void run() {
                Pools.free(SlideTransition.this);
            }
        }), Actions.removeActor()));


    }
}
