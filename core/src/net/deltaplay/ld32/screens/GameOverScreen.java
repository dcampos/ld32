package net.deltaplay.ld32.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import net.deltaplay.ld32.Assets;
import net.deltaplay.ld32.LD32;
import net.deltaplay.ld32.screens.transitions.Transitions;

public class GameOverScreen extends BaseScreen {
    private float loadingTime;
    private ProgressBar progressBar;

    public GameOverScreen(LD32 game) {
        super(game, false);

        Gdx.input.setInputProcessor(new InputMultiplexer(stage));
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (Gdx.input.justTouched()) {
            //game.clearScreens();
            game.setScreen(LD32.GAME_SCREEN, Transitions.TRANSPARENT);
        }
    }

    @Override
    public void init() {
        Label gameOver = new Label("GAME OVER", new LabelStyle(Assets.akashi48, Color.GRAY));
        Label startButton = new Label("Click to restart", new LabelStyle(Assets.akashi48, Color.valueOf("FF822E")));
        startButton.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(1.0f), Actions.fadeIn(1.0f))));
        table.add(gameOver).expandX().row();
        table.add(startButton).expandX().padTop(20);
        table.setFillParent(true);
        outerTable.debug();
        stage.addActor(table);
    }
}
