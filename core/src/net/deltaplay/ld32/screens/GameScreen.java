package net.deltaplay.ld32.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import net.deltaplay.ld32.Assets;
import net.deltaplay.ld32.InputHandler;
import net.deltaplay.ld32.LD32;
import net.deltaplay.ld32.controller.WorldController;
import net.deltaplay.ld32.model.Antenna;
import net.deltaplay.ld32.model.Entity;
import net.deltaplay.ld32.model.Ship;
import net.deltaplay.ld32.model.World;
import net.deltaplay.ld32.model.World.WorldListener;
import net.deltaplay.ld32.screens.transitions.Transitions;
import net.deltaplay.ld32.view.WorldView;

/**
 * Created by dpc on 15/04/15.
 */
public class GameScreen extends BaseScreen implements WorldListener {
    private static final int UPDATES_PER_SECOND = 30;

    public enum GameState {
        PAUSED, RUNNING, FINISHED
    }

    public GameState state;

    private World world;
    private WorldView view;
    private WorldController controller;

    private float accum;

    private ShapeRenderer debugRenderer;
    private boolean drawDebug;
    private Image lifeFull;
    private Image batteryFull;
    private Label scoreLabel;

    public InputHandler handler;

    Timer radarSoundTimer;
    Task radarSoundTask;
    Timer shipSoundTimer;
    Task shipSoundTask;

    public GameScreen(LD32 ld32) {
        super(ld32, false);

        world = new World(LD32.SCREEN_WIDTH, LD32.SCREEN_HEIGHT);
        view = new WorldView(world, LD32.SCREEN_WIDTH, LD32.SCREEN_HEIGHT);
        stage.addActor(view);

        controller = new WorldController(world, this);
        debugRenderer = new ShapeRenderer();
        drawDebug = false;

        createHUD();

        radarSoundTimer = new Timer();
        radarSoundTask = new Task() {
            @Override
            public void run() {
                Assets.radarSound.play(0.1f);
            }
        };
        shipSoundTimer = new Timer();
        shipSoundTask = new Task() {
            @Override
            public void run() {
                Assets.shipSound.play(0.3f);
            }
        };

        state = GameState.RUNNING;

        handler = new InputHandler(stage, controller);
    }

    private void createHUD() {
        Table table = new Table();
        Image lifeButton = new Image(Assets.lifeEmpty);
        Image batteryButton = new Image(Assets.batteryEmpty);
        lifeFull = new Image(Assets.lifeFull);
        batteryFull = new Image(Assets.batteryFull);
        lifeButton.setColor(Color.BLACK);
        lifeFull.setColor(Color.BLACK);
        lifeFull.setColor(Color.valueOf("00000064"));
        batteryButton.setColor(Color.BLACK);
        batteryFull.setColor(Color.valueOf("00000064"));
        batteryFull.setScaleX(0.5f);
        lifeFull.setScaleX(0.5f);

        scoreLabel = new Label("00000", new LabelStyle(Assets.scoreFont, Color.BLACK));

        table.setPosition(0, world.height - World.BASE_LINE);
        table.pad(10);
        table.setWidth(world.width);
        table.setHeight(world.BASE_LINE);
        table.stack(lifeButton, lifeFull).uniform().left();
        table.add(scoreLabel).expandX();
        table.stack(batteryButton, batteryFull).uniform().right();
        stage.addActor(table);

    }

    private void update(float delta) {
        controller.update(delta);
    }

    @Override
    public void show() {
        super.show();

        Gdx.input.setInputProcessor(new InputMultiplexer(handler, stage));
    }

    @Override
    public void render(float delta) {
        switch (state) {
            case PAUSED:
                break;
            case FINISHED:
                break;

            default:
                renderRunning(delta);
                break;
        }

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        stage.act(delta);
        stage.draw();

        renderDebug(delta);
    }

    public void renderRunning(float delta) {
        accum += delta;

        while (accum > 1.0f / UPDATES_PER_SECOND) {
            update(1.0f / UPDATES_PER_SECOND);
            accum -= 1.0f / UPDATES_PER_SECOND;
        }

        updateHUD(delta);
    }

    private void updateHUD(float delta) {
        lifeFull.setScaleX(world.ship.life / Ship.DEF_LIFE);
        batteryFull.setScaleX(world.ship.battery / Ship.DEF_BATTERY);


        String score = String.valueOf((int) world.score);
        while (score.length() < 5) {
            score = "0" + score;
        }

        scoreLabel.setText(score);
    }

    private void renderDebug(float delta) {
        debugRenderer.begin(ShapeType.Line);
        debugRenderer.polygon(world.ship.getPolygon().getVertices());
        debugRenderer.line(world.ship.antenna.startPoint, world.ship.antenna.endPoint);
        for (Entity entity : world.entities) {
            debugRenderer.polygon(entity.getPolygon().getVertices());
            if (entity instanceof Antenna) {
                Antenna ant = (Antenna) entity;
                debugRenderer.line(ant.startPoint, ant.endPoint);

            }
        }
        debugRenderer.end();
    }

    @Override
    protected void init() {

    }

    void disableAntennaSound() {
        for (int i = 0; i < world.entities.size; i++) {
            Entity ent = world.entities.get(i);
            if (ent instanceof Antenna) {
                if (((Antenna) ent).active) {
                    return;
                }

            }
        }
        radarSoundTimer.stop();
    }

    @Override
    public void entityAdded(Entity entity) {
        if (entity instanceof Antenna) {
            if (!radarSoundTask.isScheduled())
                radarSoundTimer.scheduleTask(radarSoundTask, 1, 1).run();

            radarSoundTimer.start();
        }
    }

    @Override
    public void entityRemoved(Entity entity) {
        if (entity instanceof Antenna) {
            disableAntennaSound();
        }
    }

    @Override
    public void itemCollected(Entity entity) {
        Assets.pickupSound.play(0.5f);
    }

    @Override
    public void shipStartedFiring() {
        if (!shipSoundTask.isScheduled())
            shipSoundTimer.scheduleTask(shipSoundTask, 1, 1).run();
        shipSoundTimer.start();
    }

    @Override
    public void shipStoppedFiring() {
        shipSoundTimer.stop();
    }

    @Override
    public void shipDestroyed() {
        this.state = GameState.FINISHED;
        shipSoundTimer.stop();
        radarSoundTimer.stop();
        game.clearScreens();
        game.setScreen(LD32.GAME_OVER_SCREEN, Transitions.FADE_OUT);
    }

    @Override
    public void shipCollided() {
        Assets.hitSound.play(0.5f);
    }

    @Override
    public void antennaDeactivated() {
        disableAntennaSound();
    }
}
