package net.deltaplay.ld32.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.ObjectMap;
import net.deltaplay.ld32.Assets;
import net.deltaplay.ld32.model.*;
import net.deltaplay.ld32.model.Cactus.CactusType;
import net.deltaplay.ld32.model.Mine.MineType;

/**
 * Created by dpc on 18/04/15.
 */
public class WorldView extends Actor {
    World world;
    private float stateTime = 0;
    private float drawOffset = 0;

    float angle = 45;

    Sprite foreground, middleground, background;
    Sprite mountains = new Sprite(Assets.mountains);
    Sprite ship = new Sprite(Assets.ship);
    Sprite sheepSprite = new Sprite(Assets.sheep);
    Sprite antenna = new Sprite(Assets.antenna);
    Sprite wave = new Sprite(Assets.wave);

    SpriteAnimation shipWaveAnimation = new SpriteAnimation(0.25f, Assets.shipWaves1, Assets.shipWaves2);

    float backgroundPos = 0;

    ObjectMap<Cactus.CactusType, Sprite> cactusSprites = new ObjectMap<CactusType, Sprite>();
    ObjectMap<MineType, Sprite> mineSprites = new ObjectMap<MineType, Sprite>();

    public WorldView(World world, int screenWidth, int screenHeight) {
        setSize(screenWidth, screenHeight);
        this.world = world;
        cactusSprites.put(CactusType.TYPE1, new Sprite(Assets.cactus1));
        cactusSprites.put(CactusType.TYPE2, new Sprite(Assets.cactus2));
        cactusSprites.put(CactusType.TYPE3, new Sprite(Assets.cactus3));
        mineSprites.put(MineType.TYPE1, new Sprite(Assets.mine1));
        mineSprites.put(MineType.TYPE2, new Sprite(Assets.mine2));
        mineSprites.put(MineType.TYPE3, new Sprite(Assets.mine3));
    }

    public void setSize(float width, float height) {
        super.setSize(width, height);
        this.foreground = new Sprite(createTexture(width, World.BASE_LINE, Color.BLACK));
        this.middleground = new Sprite(createTexture(width, MathUtils.ceil(height / 2 - World.BASE_LINE), Color.valueOf("FF822E")));
        this.background = new Sprite(createTexture(width, height, Color.valueOf("FF9955")));

        mountains.setPosition(backgroundPos, this.middleground.getHeight() - 1);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();

        background.draw(batch);
        middleground.draw(batch);
        foreground.draw(batch);

        backgroundPos -= MathUtils.ceil(world.speed / 2 * Gdx.graphics.getDeltaTime());

        if (backgroundPos < -mountains.getWidth()) backgroundPos = 0;

        mountains.setX(backgroundPos);
        mountains.draw(batch);
        mountains.setX(backgroundPos + mountains.getWidth() - 1);
        mountains.draw(batch);

        ship.setPosition(world.ship.position.x, world.ship.position.y);
        ship.draw(batch);

        drawWave(world.ship.antenna, batch, true);

        Sprite sprite;
        Sprite waveSprite;
        for (Entity entity : world.entities) {
            if (entity instanceof Cactus) {
                sprite = cactusSprites.get(((Cactus) entity).type);
            } else if (entity instanceof Mine) {
                sprite = mineSprites.get(((Mine) entity).type);
                sprite.setRotation(((Mine) entity).angle);
            } else if (entity instanceof Sheep) {
                sprite = sheepSprite;
                if (((Sheep)entity).flip) sprite.setFlip(true, false);
            } else {
                sprite = antenna;
                drawWave((Antenna) entity, batch, false);
            }
            sprite.setPosition(entity.position.x, entity.position.y);
            sprite.draw(batch);
        }


        if (Gdx.input.justTouched()) {
        }

        drawOffset += Gdx.graphics.getDeltaTime();
        if (drawOffset > 1) drawOffset = 0;
    }

    public void drawWave(Antenna ant, Batch batch, boolean down) {
        if (!ant.active) return;

        float startScale = 1f + drawOffset * 0.05f;
        Vector2 startPos;
        if (down) {
            startPos = new Vector2(ant.position.x, ant.position.y - ant.height - drawOffset * wave.getHeight() * 2);
        } else {
            startPos = new Vector2(ant.position.x, ant.position.y + ant.height + drawOffset * wave.getHeight() * 2);
        }

        while (world.bounds.contains(startPos) || world.bounds.contains(startPos.cpy().add(wave.getWidth(), wave.getHeight()))) {
            if (ant.angle > 0) {
                wave.setRotation(angle);

                Vector2 intersection = new Vector2();
                boolean intersects = Intersector.intersectLines(ant.startPoint, ant.endPoint, startPos, new Vector2(getWidth(), startPos.y), intersection);
                if (intersects) {
                    startPos.set(intersection);
                }
            }
            wave.setPosition(startPos.x + ant.width / 2 - wave.getWidth() / 2, startPos.y);
            wave.setScale(startScale);
            wave.draw(batch);
            startScale += 0.05f;
            if (down) {
                startPos.sub(0, wave.getHeight() * 2);
            } else {
                startPos.add(0, wave.getHeight() * 2);
            }
        }

        wave.setRotation(0);
    }

    public Texture createTexture(float tw, float th, Color color) {
        Pixmap pixmap = new Pixmap((int) tw, (int) th, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fill();
        return new Texture(pixmap);
    }


    public class SpriteAnimation {
        float duration;
        private float totalDuration;
        Sprite[] sprites;

        public SpriteAnimation(float duration, TextureRegion... regions) {
            this.duration = duration;
            this.sprites = new Sprite[regions.length];
            for (int i = 0; i < regions.length; i++) {
                sprites[i] = new Sprite(regions[i]);
            }
            this.totalDuration = duration * (sprites.length);
        }

        public Sprite getFrame(float stateTime) {
            float mod = stateTime % totalDuration;
            int key = (int) (mod / totalDuration * sprites.length);
            return sprites[key];
        }
    }
}
