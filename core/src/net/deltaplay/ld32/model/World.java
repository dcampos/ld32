package net.deltaplay.ld32.model;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Created by dpc on 18/04/15.
 */
public class World extends Entity<Rectangle> {
    public static final float BASE_LINE = 52;
    public static final float DEF_SPEED = 180;
    public static final float ANTENNA_BONUS = 25;
    public static final float SHEEP_BONUS = 50;

    public float speed = DEF_SPEED;

    public Ship ship;
    public Array<Entity> entities;

    public float score;

    public World(int screenWidth, int screenHeight) {
        this.width = screenWidth;
        this.height = screenHeight;
        bounds = new Rectangle(0, 0, screenWidth, screenHeight);

        this.ship = new Ship(BASE_LINE, (height - BASE_LINE) / 2 + Ship.DEF_HEIGHT / 2);
        this.entities = new Array<Entity>();
        this.score = 0;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void updateBounds() {
        bounds.setPosition(position);

    }

    public interface WorldListener {
        void entityAdded(Entity entity);
        void entityRemoved(Entity entity);
        void itemCollected(Entity entity);
        void shipStartedFiring();
        void shipStoppedFiring();
        void shipDestroyed();
        void shipCollided();
        void antennaDeactivated();
    }
}
