package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by dpc on 18/04/15.
 */
public class Cactus extends Entity<Rectangle> {
    public static final float DEF_DAMAGE = 1F;
    public CactusType type;


    public enum CactusType {
        TYPE1(64, 108), TYPE2(64, 72), TYPE3(36, 72);

        float width, height;

        CactusType(float w, float h) {
            this.width = w;
            this.height = h;
        }

        public static CactusType random() {
            int rand = MathUtils.random(CactusType.values().length-1);
            return CactusType.values()[rand];
        }
    }

    public Cactus(CactusType type, float x, float y) {
        position.set(x, y);
        this.type = type;
        this.width = type.width;
        this.height = type.height;
        this.bounds = new Rectangle(x, y, width, height);
        this.damage = DEF_DAMAGE;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void updateBounds() {
        bounds.setPosition(position);
    }
}
