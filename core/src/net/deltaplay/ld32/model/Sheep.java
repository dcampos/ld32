package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by dpc on 20/04/15.
 */
public class Sheep extends Entity<Rectangle> {
    public static final float DEF_WIDTH = 100;
    public static final float DEF_HEIGHT = 60;
    public static final float DEF_RESCUE = 0.5f;
    public boolean rescued;
    public boolean flip;

    public Sheep(float x, float y) {
        position.set(x, y);
        width = DEF_WIDTH;
        height = DEF_HEIGHT;
        bounds = new Rectangle(x, y, width, height);
        rescued = false;
        flip = MathUtils.random(1) == 0;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void updateBounds() {
        bounds.setPosition(position);
    }
}
