package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import net.deltaplay.ld32.LD32;

/**
 * Created by dpc on 18/04/15.
 */
public class Ship extends Entity<Rectangle> {
    public static final float DEF_WIDTH = 234;
    public static final float DEF_HEIGHT = 96;
    public static final float ANTENNA_WIDTH = 32;
    public static final float ANTENNA_HEIGHT = 0;
    public static final float DEF_DISCHARGE = 2;
    public static final float DEF_RECHARGE = 0.3f;
    public static final float DEF_LIFE = 10f;
    public static final float DEF_BATTERY = 8f;
    public static final float MIN_BATTERY = 1.5f;
    public static final float MIN_ALTITUDE = World.BASE_LINE;
    public static final float MAX_ALTITUDE = LD32.SCREEN_HEIGHT - World.BASE_LINE - DEF_HEIGHT;
    public static final float DEF_DAMAGE_TIME = 2f;

    public Antenna antenna;

    public float battery;

    public float speed;

    public float damageTime;

    public Ship(float x, float y) {
        position.set(x, y);
        width = DEF_WIDTH;
        height = DEF_HEIGHT;
        bounds = new Rectangle(x, y, width, height);
        antenna = new Antenna(x + width - ANTENNA_WIDTH, y);
        antenna.width = ANTENNA_WIDTH;
        antenna.height = ANTENNA_HEIGHT;
        antenna.active = false;
        antenna.damage = 5f;
        life = DEF_LIFE;
        battery = DEF_BATTERY;
        speed = 100f;
        moving = false;
        damageTime = 0;
    }

    public void applyDamage(float damage) {
        this.life -= damage;
        damageTime = DEF_DAMAGE_TIME;
    }

    public void applyDischarge(float delta) {
        this.battery -= delta * DEF_DISCHARGE;
        if (battery < MIN_BATTERY) antenna.active = false;
    }

    public void applyRecharge(float delta) {
        this.battery = MathUtils.clamp(battery + delta * DEF_RECHARGE, 0, DEF_BATTERY);
    }

    @Override
    public void update(float delta) {
        if (damageTime > 0) {
            damageTime -= delta;
            if (damageTime <= 0) damageTime = 0;
        }
    }

    @Override
    public void updateBounds() {
        position.y = MathUtils.clamp(position.y, MIN_ALTITUDE, MAX_ALTITUDE);
        bounds.setPosition(position);
        antenna.position.set(position.x + width - ANTENNA_WIDTH, position.y);
        antenna.updateBounds();
        antenna.startPoint.set(antenna.position.x + antenna.width / 2, antenna.position.y);
    }

}
