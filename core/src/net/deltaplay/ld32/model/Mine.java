package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by dpc on 18/04/15.
 */
public class Mine extends Entity<Rectangle> {
    public MineType type;
    public float angle;

    public enum MineType {
        TYPE1(64, 64), TYPE2(74, 74), TYPE3(84, 84);

        float width, height;

        MineType(float w, float h) {
            this.width = w;
            this.height = h;
        }

        public static MineType random() {
            int rand = MathUtils.random(MineType.values().length-1);
            return MineType.values()[rand];
        }
    }

    public Mine(MineType type, float x, float y) {
        position.set(x, y);
        this.type = type;
        this.width = type.width;
        this.height = type.height;
        this.bounds = new Rectangle(x, y, width, height);
        this.angle = 0;
    }

    @Override
    public void update(float delta) {
        angle += delta * 100;
        if (angle > 360) angle = 0;
    }

    @Override
    public void updateBounds() {
        bounds.setPosition(position);
    }
}
