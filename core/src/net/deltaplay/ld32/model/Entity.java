package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity<T extends Shape2D> {
    public Vector2 position = new Vector2();
    public float width;
    public float height;
    public T bounds;
    public float damage;
    public float life;
    public boolean moving = true;

    private Polygon poly;

    public float getDamage() {
        return damage;
    }

    public T getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public abstract void update(float delta);

    public abstract void updateBounds();

    public void applyDamage(float damage) {
        this.life -= damage;
        if (life < 0) life = 0;
        //Log.log(this.getClass(), "damage applyed=" + damage + ", life=" + life);
    }


    public Polygon getPolygon() {
        if (poly == null) poly = new Polygon();

        poly.setVertices(
                new float[]{position.x, position.y,
                        position.x + width, position.y,
                        position.x + width, position.y + height,
                        position.x, position.y + height
                }
        );
        return poly;
    }

}
