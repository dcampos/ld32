package net.deltaplay.ld32.model;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by dpc on 18/04/15.
 */
public class Antenna extends Entity<Rectangle> {
    public static final float DEF_WIDTH = 132;
    public static final float DEF_HEIGHT = 76;

    public Vector2 endPoint = new Vector2();
    public Vector2 startPoint = new Vector2();
    public float angle;

    public boolean active;

    public float damage = 4f;

    public Antenna(float x, float y) {
        position.set(x, y);
        width = DEF_WIDTH;
        height = DEF_HEIGHT;
        bounds = new Rectangle(x, y, width, height);
        startPoint.set(x + width / 2, y);
        active = true;
        angle = -1;
        life = 5f;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void updateBounds() {
        bounds.setPosition(position);
    }

    public void updatePositions() {
        startPoint.set(position.x + width / 2, position.y);
        endPoint.set(position.x + width / 2, height * 100);
    }

    @Override
    public void applyDamage(float damage) {
        super.applyDamage(damage);
        if (life <= 0) { active = false; }
    }
}
