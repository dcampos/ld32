package net.deltaplay.ld32;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

public class Assets {
    public static final String ATLAS_PATH = "data/mdpi.atlas";
    public static final String SCORE_FONT_PATH = "data/wwdigital32.fnt";
    public static final String AKASHI_FONT_PATH = "data/font.fnt";
    private static final String RADAR_SOUND_PATH = "data/sfx/radar4.wav";
    private static final String SHIP_SOUND_PATH = "data/sfx/ship1.wav";
    private static final String PICKUP_SOUND_PATH = "data/sfx/pickup2.wav";
    private static final String HIT_SOUND_PATH = "data/sfx/hit1.wav";
    public static TextureAtlas gameAtlas;
    public static TextureRegion mountains;
    public static TextureRegion mine1;
    public static TextureRegion mine2;
    public static TextureRegion mine3;
    public static TextureRegion cactus1;
    public static TextureRegion cactus2;
    public static TextureRegion cactus3;
    public static TextureRegion ship;
    public static TextureRegion sheep;
    public static TextureRegion wave;
    public static TextureRegion antenna;
    public static TextureRegion batteryFull;
    public static TextureRegion batteryEmpty;
    public static TextureRegion lifeFull;
    public static TextureRegion lifeEmpty;

    private static ObjectMap<String, TextureRegion> gameTextures;

    public static AssetManager manager = new AssetManager();
    public static BitmapFont scoreFont;
    public static BitmapFont akashi48;
    public static TextureRegion shipWaves1;
    public static TextureRegion shipWaves2;
    public static Sound radarSound;
    public static Sound shipSound;
    public static Sound pickupSound;
    public static Sound hitSound;

    static {
        int screenDpi = DisplayUtils.getScreenDPI();
    }

    public static void loadAll() {
        manager.load(ATLAS_PATH, TextureAtlas.class);
        manager.load(SCORE_FONT_PATH, BitmapFont.class);
        manager.load(AKASHI_FONT_PATH, BitmapFont.class);
        manager.load(RADAR_SOUND_PATH, Sound.class);
        manager.load(SHIP_SOUND_PATH, Sound.class);
        manager.load(PICKUP_SOUND_PATH, Sound.class);
        manager.load(HIT_SOUND_PATH, Sound.class);
   }

    public static void createAssets() {
        gameAtlas = manager.get(ATLAS_PATH, TextureAtlas.class);
        scoreFont = manager.get(SCORE_FONT_PATH, BitmapFont.class);
        akashi48 = manager.get(AKASHI_FONT_PATH, BitmapFont.class);
        mountains = gameAtlas.findRegion("mountains");
        cactus1 = gameAtlas.findRegion("cactus1");
        cactus2 = gameAtlas.findRegion("cactus2");
        cactus3 = gameAtlas.findRegion("cactus3");
        mine1 = gameAtlas.findRegion("mine1");
        mine2 = gameAtlas.findRegion("mine2");
        mine3 = gameAtlas.findRegion("mine3");
        ship = gameAtlas.findRegion("ship");
        sheep = gameAtlas.findRegion("sheep");
        antenna = gameAtlas.findRegion("antenna");
        batteryFull = gameAtlas.findRegion("battery-full");
        batteryEmpty = gameAtlas.findRegion("battery-empty");
        lifeFull = gameAtlas.findRegion("life-full");
        lifeEmpty = gameAtlas.findRegion("life-empty");
        wave = gameAtlas.findRegion("wave");
        shipWaves1 = gameAtlas.findRegion("ship-waves1");
        shipWaves2 = gameAtlas.findRegion("ship-waves2");
        radarSound = manager.get(RADAR_SOUND_PATH, Sound.class);
        shipSound = manager.get(SHIP_SOUND_PATH, Sound.class);
        pickupSound = manager.get(PICKUP_SOUND_PATH, Sound.class);
        hitSound = manager.get(HIT_SOUND_PATH, Sound.class);
    }

    public static void dispose() {
        manager.clear();
        gameAtlas.dispose();
        // purisa40.dispose();
   }
}
