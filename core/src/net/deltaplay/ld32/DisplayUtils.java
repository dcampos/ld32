package net.deltaplay.ld32;

import com.badlogic.gdx.Gdx;

public class DisplayUtils {
    public static final int MDPI = 1;
    public static final int HDPI = 2;
    public static final int XHDPI = 3;

    public static float sizeToDpi(float n) {
        switch (getScreenDPI()) {
        case HDPI:
            return n * 1.5f;

        case XHDPI:
            return n * 2.0f;

        default:
            return n;
        }
    }

    public static int getScreenDPI() {
        float density = Gdx.graphics.getDensity();
        if (density < 1.2f) {
            return MDPI;
        } else if (density > 1.8f) {
            return XHDPI;
        }
        return HDPI;
    }

}
