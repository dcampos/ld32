package net.deltaplay.ld32.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.*;
import net.deltaplay.ld32.Log;
import net.deltaplay.ld32.model.*;
import net.deltaplay.ld32.model.Cactus.CactusType;
import net.deltaplay.ld32.model.Mine.MineType;
import net.deltaplay.ld32.model.World.WorldListener;

import java.util.Iterator;

/**
 * Created by dpc on 18/04/15.
 */
public class WorldController extends InputAdapter {
    private static final float SPAWN_TIME = 3;
    public World world;

    private float spawnTime = SPAWN_TIME;
    private float sheepSpawnTime = 0;
    private float mineSpawnTime;
    private float antennaSpawnTime;
    private float cactusSpawnTime;

    public WorldListener listener;

    private int lastY = -1;
    private float firstY = -1;

    private boolean moving = false;

    public WorldController(World world, WorldListener listener) {
        this.world = world;
        this.listener = listener;
    }

    public void update(float delta) {
        generateEntities(delta);
        updateEntities(delta);
        updateShip(delta);
    }

    private void updateEntities(float delta) {

        Ship ship = world.ship;

        Iterator<Entity> iterator = world.entities.iterator();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            if (entity.moving) {
                entity.position.sub(world.speed * delta, 0);
            }
            entity.updateBounds();

            Rectangle bounds = (Rectangle) entity.bounds;

            if (entity instanceof Antenna) {
                Antenna antenna = (Antenna) entity;
                antenna.updatePositions();

                //Rectangle waveRect = new Rectangle(antenna.startPoint.x - antenna.width / 2, antenna.startPoint.y, antenna.width, antenna.height * 100);

                if (antenna.active && Intersector.intersectSegmentPolygon(antenna.startPoint, antenna.endPoint, ship.getPolygon())) {
                    ship.applyDamage(antenna.damage * delta);
                }

                Polygon poly = antenna.getPolygon();

                if (Intersector.intersectSegmentPolygon(ship.antenna.startPoint, ship.antenna.endPoint, poly)) {
                    if (ship.antenna.active && antenna.active && ship.battery > 0) {
                        antenna.applyDamage(ship.antenna.damage * delta);
                        if (!antenna.active) {
                            world.score += World.ANTENNA_BONUS;
                            listener.antennaDeactivated();
                        }
                    }
                }

                if (bounds.overlaps(ship.bounds)) {
                    ship.life = 0;
                    listener.shipCollided();
                }
            } else if (entity instanceof Mine) {
                if (bounds.overlaps(ship.bounds)) {
                    ship.life = 0;
                    listener.shipCollided();
                }
                entity.update(delta);
            } else if (entity instanceof Cactus) {
                if (bounds.overlaps(ship.bounds) && ship.damageTime == 0) {
                    ship.applyDamage(((Cactus)entity).damage*delta);
                    listener.shipCollided();
                }
                entity.update(delta);
            } else if (entity instanceof Sheep) {
                Sheep sheep = (Sheep) entity;
                if (sheep.rescued) {
                    if (sheep.position.y > ship.position.y) {
                        iterator.remove();
                        world.score += World.SHEEP_BONUS;
                        listener.entityRemoved(sheep);
                    } else {
                        sheep.position.y += world.speed*delta;
                    }
                } else if (sheep.position.x > ship.position.x && sheep.position.x + sheep.width < ship.position.x + ship.width
                        && ship.position.y <= sheep.position.y + sheep.height) {
                    listener.itemCollected(sheep);
                    sheep.rescued = true;
                    sheep.moving = false;
                }
            }

            if (world.bounds.x > bounds.getX() + bounds.getWidth() * 2) {
                iterator.remove();
                listener.entityRemoved(entity);
                Log.log(this.getClass(), "entity removed=" + entity.getClass().getName());
            }

        }

        if (ship.life <= 0) listener.shipDestroyed();

        float meters = delta * World.DEF_SPEED;

        world.score += meters / 100;
    }

    private void updateShip(float delta) {
        Ship ship = world.ship;

        ship.update(delta);

        if ((moving && lastY < firstY) || Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DPAD_DOWN)) {
            ship.position.sub(0, delta * ship.speed);
        } else if ((moving && lastY >= firstY) || Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.DPAD_UP)) {
            ship.position.add(0, delta * ship.speed);
        }

        ship.updateBounds();

        if (ship.antenna.active) {
            ship.applyDischarge(delta);
        }

        ship.applyRecharge(delta);
    }

    private void generateEntities(float delta) {
        spawnTime += delta;
        sheepSpawnTime += delta;
        mineSpawnTime += delta;
        antennaSpawnTime += delta;
        cactusSpawnTime += delta;

        float rand = MathUtils.random(1f);
        Entity entity = null;
        if (spawnTime > SPAWN_TIME) {


            if (rand > 0.95f && cactusSpawnTime > 10) {
                Cactus cactus = new Cactus(CactusType.random(), world.width, World.BASE_LINE);
                entity = cactus;
            } else if (rand > 0.89f && mineSpawnTime > 2) {
                Antenna antenna = new Antenna(world.width, World.BASE_LINE);
                antenna.endPoint.set(antenna.position.x, world.height);
                entity = antenna;
                antennaSpawnTime = 0;
            } else if (rand < 0.5f && sheepSpawnTime > 20) {
                Sheep sheep = new Sheep(world.width, World.BASE_LINE);
                entity = sheep;
                sheepSpawnTime = 0;
            }
        } else if (rand > 0.85f && mineSpawnTime > 7 && antennaSpawnTime > 2) {
            Mine mine = new Mine(MineType.random(), world.width, world.height - World.BASE_LINE * MathUtils.random(1.5f, 3.5f));
            entity = mine;
            mineSpawnTime = 0;
        }

        if (entity != null) {
            world.entities.add(entity);
            listener.entityAdded(entity);
            spawnTime = 0;
        }

    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Ship ship = world.ship;

        //float touchX = Gdx.input.getX();
        //float touchY = world.height - Gdx.input.getY();
        float touchX = screenX;
        float touchY = screenY;

        if (!moving && ship.bounds.contains(touchX, touchY)) {
            moving = true;
            firstY = (int)touchY;
            return true;
        } else if (moving) {
            lastY = (int)touchY;
            return true;
        }

        Vector2 p1 = new Vector2(ship.position.x + ship.width, ship.position.y);
        Vector2 p2 = new Vector2(touchX, touchY);//world.height - Gdx.input.getY());

        float lenAB = (float) Math.sqrt(Math.pow((double) p2.x - (double) p1.x, 2) + Math.pow((double) p1.y - (double) p2.y, 2));

        Vector2 p3 = new Vector2();
        p3.x = p2.x + (p2.x - p1.x) / lenAB * world.width;
        p3.y = p2.y + (p2.y - p1.y) / lenAB * world.width;

        float degrees = calculateAngle(p1, p3) + 90;

        if (degrees > 360) degrees -= 360;

        if (degrees >= 0 && degrees < 70 && ship.battery > Ship.MIN_BATTERY) {
            ship.antenna.angle = degrees;
            ship.antenna.endPoint = p3;
            if (!ship.antenna.active) {
                listener.shipStartedFiring();
                ship.antenna.active = true;
            }
        }

        //System.err.println("angle=" + degrees);

        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Ship ship = world.ship;
        listener.shipStoppedFiring();
        if (moving) moving = false;
        if (ship.antenna.active) {
            ship.antenna.active = false;
        }
        return super.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
            touchDown(screenX, screenY, pointer, Buttons.LEFT);
        }
        return super.touchDragged(screenX, screenY, pointer);
    }

    public float calculateAngle(Vector2 point1, Vector2 point2) {
        float deltaY = point2.y - point1.y;
        float deltaX = point2.x - point1.x;
        float degrees = MathUtils.atan2(deltaY, deltaX) * 180 / MathUtils.PI;
        if (degrees < 0) degrees += 360;
        return degrees;
    }
}
