package net.deltaplay.ld32;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.deltaplay.ld32.controller.WorldController;

/**
 * Created by dpc on 27/04/15.
 */
public class InputHandler extends InputAdapter {
    public Stage stage;
    public WorldController controller;
    private Vector3 touchPosition;

    public InputHandler(Stage stage, WorldController controller) {
        this.stage = stage;
        this.controller = controller;
        this.touchPosition = new Vector3();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        updateTouch(screenX, screenY);
        controller.touchDown((int)touchPosition.x, (int)touchPosition.y, pointer, button);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        updateTouch(screenX, screenY);
        controller.touchUp((int) touchPosition.x, (int) touchPosition.y, pointer, button);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        updateTouch(screenX, screenY);
        controller.touchDragged((int) touchPosition.x, (int) touchPosition.y, pointer);
        return true;
    }

    private void updateTouch(int screenX, int screenY) {
        touchPosition.set(screenX, screenY, 0);
        touchPosition = stage.getViewport().unproject(touchPosition);
    }
}
